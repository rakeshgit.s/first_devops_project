package com.avitech.devopsintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class DevopsIntegrationApplication {

    @GetMapping("/")
    public String message() {
        return "devops-integeration project is running >>>>>>>";
    }

    public static void main(String[] args) {

        SpringApplication.run(DevopsIntegrationApplication.class, args);
    }

}
